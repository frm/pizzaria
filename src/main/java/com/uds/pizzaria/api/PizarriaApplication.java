package com.uds.pizzaria.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizarriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizarriaApplication.class, args);
	}

}
