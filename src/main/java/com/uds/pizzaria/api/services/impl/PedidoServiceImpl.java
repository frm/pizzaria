package com.uds.pizzaria.api.services.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uds.pizzaria.api.entities.Pedido;
import com.uds.pizzaria.api.repositories.PedidoRepository;
import com.uds.pizzaria.api.services.PedidoService;

@Service
public class PedidoServiceImpl implements PedidoService {
	
	
	private static final Logger log = LoggerFactory.getLogger(PedidoServiceImpl.class);

	@Autowired
	private PedidoRepository pedidoRepository;

	@Override
	public Pedido persistir(Pedido pedido) {
		log.info("Persistindo pedido: {}", pedido);
		BigDecimal valorTotal = calculaValorTotal(pedido).setScale(2);
		BigDecimal tempoTotal = calculaTempoTotal(pedido);
		pedido.setTempoTotal(tempoTotal);
		pedido.setValorTotal(valorTotal);
		return this.pedidoRepository.save(pedido);
	}

	private BigDecimal calculaTempoTotal(Pedido pedido) {
		BigDecimal tempoTotal = BigDecimal.ZERO;
		if(!pedido.getPizza().getAdicional().isEmpty()) {
			pedido.getPizza().getAdicional().forEach(a -> {
				tempoTotal.add(a.getMinutos());
			});
		}
		tempoTotal.add(pedido.getPizza().getTamanho().getMinutos());
		tempoTotal.add(pedido.getPizza().getSabor().getMinutos());
		return tempoTotal;
	}

	private BigDecimal calculaValorTotal(Pedido pedido) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		if(!pedido.getPizza().getAdicional().isEmpty()) {
			pedido.getPizza().getAdicional().forEach(a -> {
				valorTotal.add(a.getPreco());
			});
		}
		valorTotal.add(pedido.getPizza().getTamanho().getPreco());
		return valorTotal;
	}

}
