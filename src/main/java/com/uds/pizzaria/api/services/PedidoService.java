package com.uds.pizzaria.api.services;

import com.uds.pizzaria.api.entities.Pedido;

public interface PedidoService {

	/**
	 * Cadastra uma nova pizza na base de dados.
	 * 
	 * @param pizza
	 * @return Pizza
	 */
	Pedido persistir(Pedido pedido);

}
