package com.uds.pizzaria.api.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import com.uds.pizzaria.api.enums.Adicional;
import com.uds.pizzaria.api.enums.Sabor;
import com.uds.pizzaria.api.enums.Tamanho;

@Entity
@Table(name = "pizzas")
public class Pizza implements Serializable {


	private static final long serialVersionUID = 146859729342937219L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_pizza")
	private Long id;

	@Enumerated(EnumType.STRING)
	@NonNull
	@Column(name = "sabor", nullable = false)
	private Sabor sabor;
	
	@Enumerated(EnumType.STRING)
	@NonNull
	@Column(name = "tamanho", nullable = false)
	private Tamanho tamanho;
	
	@ElementCollection
	@CollectionTable(name = "adiciona")
	@Enumerated(EnumType.STRING)
	private List<Adicional> adicional;


	public Pizza() {
	}


	public Sabor getSabor() {
		return sabor;
	}


	public void setSabor(Sabor sabor) {
		this.sabor = sabor;
	}
	@ElementCollection

	public Tamanho getTamanho() {
		return tamanho;
	}


	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}



	public List<Adicional> getAdicional() {
		return adicional;
	}


	public void setAdicional(List<Adicional> adicional) {
		this.adicional = adicional;
	}


	public Long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sabor == null) ? 0 : sabor.hashCode());
		result = prime * result + ((tamanho == null) ? 0 : tamanho.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (sabor != other.sabor)
			return false;
		if (tamanho != other.tamanho)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Pizza [sabor=" + sabor + ", tamanho=" + tamanho + "]";
	}

}