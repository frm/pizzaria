package com.uds.pizzaria.api.enums;

import java.math.BigDecimal;

public enum Adicional {
	EXTRA_BACON(BigDecimal.valueOf(3),BigDecimal.ZERO), SEM_CEBOLA(BigDecimal.ZERO,BigDecimal.ZERO), BORDA_RECEHADA(BigDecimal.valueOf(5),BigDecimal.valueOf(5));
	
	private final BigDecimal preco;
	private final BigDecimal minutos;

	Adicional(BigDecimal opcaoPreco, BigDecimal opcaoMinutos){
		preco = opcaoPreco;
		minutos = opcaoMinutos;
    }
    public BigDecimal getMinutos(){
        return minutos;
    }
    
	public BigDecimal getPreco() {
		return preco;
	}
}
