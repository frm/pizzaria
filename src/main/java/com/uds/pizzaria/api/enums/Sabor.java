package com.uds.pizzaria.api.enums;


import java.math.BigDecimal;

public enum Sabor {
	CALABRESA(BigDecimal.valueOf(0)), MARGUERITA(BigDecimal.valueOf(0)), PORTUGUESA(BigDecimal.valueOf(5));
	
	private final BigDecimal minutos;
	Sabor(BigDecimal valorOpcao){
		minutos = valorOpcao;
    }
    public BigDecimal getMinutos(){
        return minutos;
    }

}
