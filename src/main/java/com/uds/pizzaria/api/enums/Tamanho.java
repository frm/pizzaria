package com.uds.pizzaria.api.enums;

import java.math.BigDecimal;

public enum Tamanho {
	PEQUENA(BigDecimal.valueOf(20),BigDecimal.valueOf(15)), MEDIA(BigDecimal.valueOf(30),BigDecimal.valueOf(20)), GRANDE(BigDecimal.valueOf(40),BigDecimal.valueOf(25));
	
	private final BigDecimal preco;
	private final BigDecimal minutos;

	Tamanho(BigDecimal opcaoPreco, BigDecimal opcaoMinutos){
		preco = opcaoPreco;
		minutos = opcaoMinutos;
    }
    public BigDecimal getMinutos(){
        return minutos;
    }
    
	public BigDecimal getPreco() {
		return preco;
	}

}
