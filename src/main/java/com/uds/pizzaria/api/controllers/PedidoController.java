package com.uds.pizzaria.api.controllers;

import java.security.NoSuchAlgorithmException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uds.pizzaria.api.entities.Pedido;
import com.uds.pizzaria.api.services.PedidoService;

@RestController
@RequestMapping("/api/pedidos")
@CrossOrigin(origins = "*")
public class PedidoController {
	
	@Autowired
	private PedidoService pedidoService;
	
	/**
	 * Cadastra um pedido no sistema.
	 * 
	 * @param pedido
	 * @param result
	 * @return ResponseEntity<Pedido>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	public ResponseEntity<Pedido> cadastrar(@Valid @RequestBody Pedido pedido) throws NoSuchAlgorithmException {
		pedidoService.persistir(pedido);
		return ResponseEntity.ok(pedido);

	}

}
